//
// BeansList.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct BeansList: Codable, JSONEncodable, Hashable {

    public var totalCount: Int
    public var pageSize: Int
    public var currentPage: Int
    public var totalPages: Int
    public var items: [Bean]

    public init(totalCount: Int, pageSize: Int, currentPage: Int, totalPages: Int, items: [Bean]) {
        self.totalCount = totalCount
        self.pageSize = pageSize
        self.currentPage = currentPage
        self.totalPages = totalPages
        self.items = items
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case totalCount
        case pageSize
        case currentPage
        case totalPages
        case items
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(totalCount, forKey: .totalCount)
        try container.encode(pageSize, forKey: .pageSize)
        try container.encode(currentPage, forKey: .currentPage)
        try container.encode(totalPages, forKey: .totalPages)
        try container.encode(items, forKey: .items)
    }
}

