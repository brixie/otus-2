//
//  Otus_2App.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 15.06.2024.
//

import SwiftUI

@main
struct Otus_2App: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
