//
//  Datasource.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 16.06.2024.
//

import Foundation

class DataSource: ObservableObject {
    @Published var beans: [Bean] = []
    
    var page = 1
    var pageSize = 20
    var hasMore = true
    
    enum State {
        case loadingNextPage
        case data
        case nextPageData
        case idle
    }
    
    var state: State = .data
    
    func fetchNextPage(beanType: BeanType, shouldReset: Bool = false) -> Void {
        if shouldReset {
            reset()
        }
        
        guard
            state == .data || state == .nextPageData,
            hasMore
        else {
            return
        }
        
        state = .loadingNextPage
        
        var kosher: Bool?
        if beanType == .kosher {
            kosher = true
        }
        var glutenFree: Bool?
        if beanType == .glutenFree {
            glutenFree = true
        }
        var seasonal: Bool?
        if beanType == .seasonal {
            seasonal = true
        }
        var sugarFree: Bool?
        if beanType == .sugarFree {
            sugarFree = true
        }
        
        DefaultAPI.getAllBeans(pageIndex: page,
                               pageSize: pageSize,
                               glutenFree: glutenFree,
                               sugarFree: sugarFree,
                               seasonal: seasonal,
                               kosher: kosher) { [weak self] data, error in
            guard let self, let data else { return }
            
            beans.append(contentsOf: data.items)
            hasMore = page <= data.totalPages

            if hasMore {
                page += 1
            }
            
            state = hasMore ? .nextPageData : .idle
        }
    }
    
    func reset() {
        beans.removeAll()
        hasMore = true
        page = 1
        state = .data
    }
}
