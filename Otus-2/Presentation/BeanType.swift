//
//  BeanType.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 16.06.2024.
//

import Foundation

enum BeanType {
    case all
    case kosher
    case seasonal
    case glutenFree
    case sugarFree
    
    var toString: String {
        switch self {
        case .all: return "All"
        case .kosher: return "Kosher"
        case .seasonal: return "Seasonal"
        case .glutenFree: return "Gluten Free"
        case .sugarFree: return "Sugar Free"
        }
    }
}
