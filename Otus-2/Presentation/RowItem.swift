//
//  RowItem.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 17.06.2024.
//

import SwiftUI

struct RowItem: View {
    
    let index: Int
    let bean: Bean
    
    var body: some View {
        RoundedRectangle(cornerRadius: 5)
            .fill(Color(hex: bean.backgroundColor))
            .overlay(
                HStack {
                    Text(bean.flavorName)
                }
            )
    }
}
